use std::error::Error;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while1},
    character::complete::{alpha1, alphanumeric1, multispace0},
    combinator::{map, map_res, recognize},
    error::{ErrorKind, ParseError},
    multi::{many0_count, separated_list1},
    sequence::{delimited, pair, separated_pair},
    AsChar, IResult, InputTakeAtPosition, Parser,
};
use std::fmt;
// parser stuff
fn ws<'a, F, O, E: ParseError<&'a str>>(inner: F) -> impl FnMut(&'a str) -> IResult<&'a str, O, E>
where
    F: FnMut(&'a str) -> IResult<&'a str, O, E>,
{
    delimited(multispace0, inner, multispace0)
}

pub fn lower1<'a, Error: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, &str, Error> {
    take_while1(|a: char| a.is_alphabetic() && a.is_lowercase())(input)
}
pub fn upper1<'a, Error: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, &str, Error> {
    take_while1(|a: char| a.is_alphabetic() && a.is_uppercase())(input)
}
pub fn name_id<'a, Error: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, &str, Error> {
    recognize(pair(lower1, many0_count(alpha1)))(input)
}

pub fn agent_id<'a, Error: ParseError<&'a str>>(input: &'a str) -> IResult<&'a str, &str, Error> {
    recognize(pair(upper1, many0_count(alpha1)))(input)
}

// A "net" is built by connetions between terms
// TODO write a parser for:
// TODO actually, I want to use rust-sitter instead...

// <term> ::= <name> | <agent>
// <name> ::= <nameID>
// <agent> ::= <agentID>
//           | <agentID> ['(' <term> ',' ... ',' <term> ')']
// terms are built on names and agents
// TODO an agent has 1 principal port + n-fixed auxiliary ports
// Agents works as a constructor and a destructor
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Agent {
    id: String,
    iports: Option<Vec<Term>>,
    oport: Option<Box<Term>>,
}
// TODO write a parser combinator in nom to parse the syntax
impl Agent {
    pub fn new_def(id: String) -> Agent {
        Agent {
            id,
            iports: None,
            oport: None,
        }
    }
    pub fn new(id: String, iports: Option<Vec<Term>>, oport: Option<Box<Term>>) -> Agent {
        Agent { id, oport, iports }
    }
    pub fn parse_no_port(input: &str) -> IResult<&str, Self> {
        // TODO use preceded to relax agent names to start with just an upper (so it doesn't have to all be upper)
        agent_id(input).map(|(o, i): (&str, &str)| {
            (
                o,
                Self {
                    id: String::from(i),
                    iports: None,
                    oport: None,
                },
            )
        })
    }

    // pub fn parse_ports(input: &str) -> IResult <&str, Self> {
    pub fn parse_port(input: &str) -> IResult<&str, Self> {
        let (lov, name): (&str, &str) = alt((agent_id, name_id))(input)?;
        // TODO I don't have a term parser yet.. but the args to this is a term
        let (lov, out) = delimited(
            tag("("),
            separated_list1(ws(tag(",")), Term::parse),
            tag(")"),
        )(lov)?;
        let (a, b) = out.split_first().unwrap(); // this should always succeed
        let ip = match b {
            b if b.len() > 0 => Some(b.to_vec()),
            _ => None,
        };
        Ok((
            lov,
            Self {
                id: String::from(name),
                iports: ip,
                oport: Some(Box::new(a.clone())),
            },
        ))
    }
    pub fn parse(input: &str) -> IResult<&str, Self> {
        alt((Self::parse_port, Self::parse_no_port))(input)
    }
}

impl fmt::Display for Agent {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        match (&self.oport, &self.iports) {
            (None, None) => write!(f, "{}", self.id),
            (Some(c), None) => write!(f, "{}({})", self.id, c),
            (Some(c), Some(i)) => {
                write!(
                    f,
                    "{}({}{})",
                    self.id,
                    c,
                    i.iter()
                        .fold(String::new(), |ac, i| ac + "," + &i.to_string())
                )
            }

            // this is a dead case but whatever
            (None, Some(i)) => write!(f, "{}", self.id),
        }
    }
}
// TODO linearity-restriction - names must occur at most / least twice
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Name {
    id: String,
}
impl Name {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        name_id(input).map(|(o, i): (&str, &str)| {
            (
                o,
                Name {
                    id: String::from(i),
                },
            )
        })
    }
}
impl fmt::Display for Name {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        write!(f, "{}", self.id)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum Term {
    N(Name),
    A(Agent),
}
impl Term {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        alt((
            Agent::parse.map(|a| Self::A(a)),
            Name::parse.map(|a| Self::N(a)),
        ))(input)
    }
}
impl fmt::Display for Term {
    // This trait requires `fmt` with this exact signature.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // Write strictly the first element into the supplied output
        // stream: `f`. Returns `fmt::Result` which indicates whether the
        // operation succeeded or failed. Note that `write!` uses syntax which
        // is very similar to `println!`.
        match self {
            Self::N(n) => write!(f, "{}", n),
            Self::A(n) => write!(f, "{}", n),
        }
    }
}
// copied to parse whitespace

// TODO You can make connections between a name and an agent

// TODO https://github.com/Logicalshift/flo_draw/tree/v0.4
// it'd be nice to draw the interaction nets as well..?

// TODO For the inpla impl, I need to be able to handle connectoins between names as well as connections between agents.
// Maybe I can just store some Map of which names are connected to what?

// TODO 2 connections with the same name on right / left hand side are commutative, so if t~x and x~s, then t~s and "x" is disposed of.

// connecting 2 agents means evaluating their *interaction rules*
#[derive(Debug, Clone)]
pub struct Connection {
    pub left: Term,
    pub right: Term,
}
// <connection> ::= <term> ~ <term>
impl Connection {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        let (lo, out) = Term::parse(input)?;
        let (lo1, _) = ws(tag("~"))(lo)?;
        let (lo2, out1) = Term::parse(lo1)?;
        return Ok((
            lo2,
            Connection {
                left: out,
                right: out1,
            },
        ));
    }
}

// A "configuration" is a set of rules + evaluating a connection..?

// <interaction-rule> ::= <rule-agent> '><' <rule-agent> '=>' <connections> ';'
// <rule-agent> ::= <agentID>
//                | <agentID> '(' <name> ',' ... ',' <name> ')'
// TODO proviso:
// Each *name* that occur between two rule-agent must be distinct
// rule-agent >< rule-agent (names in these 2 must be distinct)
// + linearity, every name in the interaction-rule must occur twice!
#[derive(Debug, Clone)]
pub struct InteractionRule {
    agent1: Agent,
    agent2: Agent,
    connections: Vec<Connection>,
}
impl InteractionRule {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        let (lo, agent1) = Agent::parse(input)?;
        let (lo1, _) = ws(tag("><"))(lo)?;
        let (lo2, agent2) = Agent::parse(lo1)?;
        let (lo3, _) = ws(tag("=>"))(lo2)?;
        let (lo4, connections) = separated_list1(ws(tag(",")), Connection::parse)(lo3)?;
        return Ok((
            lo4,
            InteractionRule {
                agent1,
                agent2,
                connections,
            },
        ));
    }
}

#[derive(Debug, Clone)]
pub enum Ast {
    T(Term),
    IN(InteractionRule),
    C(Connection),
}
impl Ast {
    pub fn parse(input: &str) -> IResult<&str, Self> {
        alt((
            InteractionRule::parse.map(Ast::IN),
            Connection::parse.map(Ast::C),
            Term::parse.map(Ast::T),
        ))(input)
    }
}
// TODO parse more stuff like
// conditional rewriting
