use crate::db::*;
use crate::parse::*;
use std::io;
use std::io::Write;

pub fn repl() {
    let mut env = ProgDB::new();
    // TODO https://github.com/nushell/reedline
    // integrate this into my code..
    loop {
        print!(">> ");
        io::stdout().flush().unwrap();
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let (o, t) = Ast::parse(input.as_str()).unwrap();
        interpret(&t, &mut env);
    }
}

// TODO I'll just have this print something for now.. not sure what to have it return yet..
pub fn interpret(ast: &Ast, env: &mut ProgDB) {
    match ast {
        Ast::C(c) => {
            // TODO a connection should be inserted into the DB
            env.insert_con(c.left.clone(), c.right.clone());
            println!("Inserted: {} : {} into the env", c.left, c.right)
        }
        Ast::IN(i) => {
            // println!("matched: {}", i)
            // TODO implement interaction rules and check the restrictions
        }
        Ast::T(t) => {
            println!("matched: {}", t);
            // TODO do some sort of type checking
            let l = env.lookup_con(t);
            match l {
                Some(te) => println!("found: {} in the env", te),
                None => println!("term undefined"),
            }
            // println!("found: {}", t);
        }
    }
}
