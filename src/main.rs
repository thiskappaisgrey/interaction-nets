mod db;
mod interpret;
mod parse;

use std::error::Error;

use interpret::repl;
use parse::Term;
fn main() -> Result<(), Box<dyn Error>> {
    // let (_, out) = Term::parse("name(r)")?;
    // dbg!("{}", out);
    repl();
    Ok(())
}
