use crate::parse::*;
use std::collections::HashMap;

// database / environment

// keep a hashmap of connections
pub struct ProgDB {
    // TODO so here, you have a DB of connections between terms?
    conns: HashMap<Term, Term>,
}
impl ProgDB {
    pub fn new() -> Self {
        ProgDB {
            conns: HashMap::new(),
        }
    }
    pub fn insert_con(&mut self, term: Term, term1: Term) -> Option<Term> {
        self.conns.insert(term, term1)
    }
    pub fn lookup_con(&self, term: &Term) -> Option<Term> {
        self.conns.get(term).cloned()
    }
}
